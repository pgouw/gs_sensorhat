#!/usr/bin/python
# Copyright (c) 2014 Adafruit Industries
# Author: Tony DiCola
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

# Can enable debug output by uncommenting:
#import logging
#logging.basicConfig(level=logging.DEBUG)
import time
import smbus

# Note that this will work with the TMP007 sensor too!  Just use the TMP006
# class below as-is.
import Adafruit_TMP.TMP006 as TMP006

bus = smbus.SMBus(1)

# Define a function to convert celsius to fahrenheit.
def c_to_f(c):
    return c * 9.0 / 5.0 + 32.0


# Default constructor will use the default I2C address (0x40) and pick a default I2C bus.
#
# For the Raspberry Pi this means you should hook up to the only exposed I2C bus
# from the main GPIO header and the library will figure out the bus number based
# on the Pi's revision.
#
# For the Beaglebone Black the library will assume bus 1 by default, which is
# exposed with SCL = P9_19 and SDA = P9_20.
#
# Remember this TMP006 code will work fine with the TMP007 sensor too!
#sensor = TMP006.TMP006()

# Optionally you can override the address and/or bus number:
sensor = TMP006.TMP006(address=0x44, busnum=1)

# Initialize communication with the sensor, using the default 16 samples per conversion.
# This is the best accuracy but a little slower at reacting to changes.
sensor.begin()

# Optionally initialize with a faster but less precise sample rate.  You can use
# any value from TMP006_CFG_1SAMPLE, TMP006_CFG_2SAMPLE, TMP006_CFG_4SAMPLE,
# TMP006_CFG_8SAMPLE, or TMP006_CFG_16SAMPLE for the sample rate.
#sensor.begin(samplerate=TMP006.CFG_1SAMPLE)


def readtsl2571():
    # TSL2571 address, 0x39(57)
    # Select enable register, 0x00(00) with command register, 0x80(128)
    #		0x0B(11)	Set Power ON, ALS Enabled
    bus.write_byte_data(0x39, 0x00 | 0x80, 0x0B)
    # TSL2571 address, 0x39(57)
    # Select ALS time register, 0x01(01) with command register, 0x80(128)
    #		0xFF(255)	Atime = 2.72 ms, Max count = 1023
    bus.write_byte_data(0x39, 0x01 | 0x80, 0xFF)
    # TSL2571 address, 0x39(57)
    # Select wait time register, 0x03(03) with command register, 0x80(128)
    #		0xFF(255)	Wtime = 2.72 ms
    bus.write_byte_data(0x39, 0x03 | 0x80, 0xFF)
    # TSL2571 address, 0x39(57)
    # Select control register, 0x0F(15) with command register, 0x80(128)
    #		0x20(32)	Gain = 1x
    bus.write_byte_data(0x39, 0x0F | 0x80, 0x20)

    time.sleep(0.5)

    # TSL2571 address, 0x39(57)
    # Read data back from 0x14(20) with command register, 0x80(128), 4 bytes
    # c0Data LSB, c0Data MSB, c1Data LSB, c1Data MSB
    data = bus.read_i2c_block_data(0x39, 0x14 | 0x80, 4)

    # Convert the data
    c0Data = data[1] * 256 + data[0]
    c1Data = data[3] * 256 + data[2]
    CPL = (2.72 * 1.0) / 53.0;
    luminance1 = (1 * c0Data - 2.0 * c1Data) / CPL
    luminance2 = (0.6 * c0Data - 1.00 * c1Data) / CPL
    luminance = 0.0
    if luminance1 > 0 and luminance2 > 0 :
	    if luminance1 > luminance2 :
	     	   luminance = luminance1
	    else :
		   luminance = luminance2

    # Output data to screen
    print "Ambient Light luminance is : %.2f lux" %luminance

def readms8607():
    time.sleep(0.5)

    # Read 12 bytes of calibration data
    # Pressure sensitivity | SENST1
    data1 = bus.read_i2c_block_data(0x76, 0xA2, 2)
    # Pressure offset | OFFT1
    data2 = bus.read_i2c_block_data(0x76, 0xA4, 2)
    # Temperature coefficient of pressure sensitivity | TCS
    data3 = bus.read_i2c_block_data(0x76, 0xA6, 2)
    # Temperature coefficient of pressure offset | TCO
    data4 = bus.read_i2c_block_data(0x76, 0xA8, 2)
    # Reference temperature | TREF
    data5 = bus.read_i2c_block_data(0x76, 0xAA, 2)
    # Temperature coefficient of the temperature | TEMPSENS
    data6 = bus.read_i2c_block_data(0x76, 0xAC, 2)

    # Convert the data
    c1 = data1[0] * 256 + data1[1]
    c2 = data2[0] * 256 + data2[1]
    c3 = data3[0] * 256 + data3[1]
    c4 = data4[0] * 256 + data4[1]
    c5 = data5[0] * 256 + data5[1]
    c6 = data6[0] * 256 + data6[1]
    # MS8607_02BA address, 0x76(118)
    #		0x40(64)	Initiate pressure conversion(OSR = 256)
    bus.write_byte(0x76, 0x40)

    time.sleep(0.5)

    # Read data back from 0x00(0), 3 bytes, D1 MSB2, D1 MSB1, D1 LSB
    # Digital pressure value
    data = bus.read_i2c_block_data(0x76, 0x00, 3)

    D1 = data[0] * 65536 + data[1] * 256 + data[2]

    # MS8607_02BA address, 0x76(118)
    #		0x50(64)	Initiate temperature conversion(OSR = 256)
    bus.write_byte(0x76, 0x50)

    time.sleep(0.5)

    # Read data back from 0x00(0), 3 bytes, D2 MSB2, D2 MSB1, D2 LSB
    # Digital temperature value
    data0 = bus.read_i2c_block_data(0x76, 0x00, 3)

    # Convert the data
    D2 = data0[0] * 65536 + data0[1] * 256 + data0[2]
    dT = D2 - c5 * 256
    Temp = 2000 + dT * c6 / 8388608
    OFF = c2 * 131072 + (c4 * dT) / 64
    SENS = c1 * 65536 + (c3 * dT ) / 128

    if Temp >= 2000 :
    	Ti = 5 * (dT * dT) / 274877906944
    	OFFi = 0
    	SENSi= 0
    elif Temp < 2000 :
    	Ti = 3 * (dT * dT) / 8589934592
    	OFFi= 61 * ((Temp - 2000) * (Temp - 2000)) / 16
    	SENSi= 29 * ((Temp - 2000) * (Temp - 2000)) / 16
    	if Temp < -1500:
    		OFFi = OFFi + 17 * ((Temp + 1500) * (Temp + 1500))
    		SENSi = SENSi + 9 * ((Temp + 1500) * (Temp +1500))
    OFF2 = OFF - OFFi
    SENS2= SENS - SENSi
    cTemp = (Temp - Ti) / 100.0
    fTemp =  cTemp * 1.8 + 32
    pressure = ((((D1 * SENS2) / 2097152) - OFF2) / 32768.0) / 100.0

    # MS8607_02BA address, 0x40(64)
    #		0xFE(254)	Send reset command
    bus.write_byte(0x40, 0xFE)

    time.sleep(0.3)

    # MS8607_02BA address, 0x40(64)
    #		0xF5(245)	Send NO Hold master command
    bus.write_byte(0x40, 0xF5)

    time.sleep(0.5)

    # MS8607_02BA address, 0x40(64)
    # Read data back from device
    data0 = bus.read_byte(0x40)
    data1 = 0

    # Convert the data
    D3 = data0 * 256 + data1

    humidity = (-6.0 + (125.0 * (D3 / 65536.0)))

    # Output data to screen
    print "Relative Humidity : %.2f %%" %humidity
    print "Temperature compensated Pressure is : %.2f mbar" %pressure
    print "Temperature in Celsius : %.2f C" %cTemp
    print "Temperature in Fahrenheit : %.2f F" %fTemp

print('Press Ctrl-C to quit.')
while True:
    obj_temp = sensor.readObjTempC()
    die_temp = sensor.readDieTempC()
    print('Object temperature: {0:0.3F}*C / {1:0.3F}*F'.format(obj_temp, c_to_f(obj_temp)))
    print('   Die temperature: {0:0.3F}*C / {1:0.3F}*F'.format(die_temp, c_to_f(die_temp)))
    time.sleep(1.0)
    readtsl2571()
    readms8607()
